(* {-# VERSION 9chsTNDS-0.1 #-} *)

let[@inline] oneYear = 31536952

type tProtocol = string
type tHashedName = bytes
type tDestType = Wallet | IP | URL
type tRecords = (tHashedName, (address * tDestType) ) map

type tMerkleProof = ((bytes, bytes) variant) list

type tTeznameInfo
  = RegularName
  | PremiumName    of tMerkleProof
  | RestrictedName of tMerkleProof * bytes
type tTeznameType
  = Regular | Premium | Restricted of bytes

type tFees =
  { sRgNameFee : tez
  ; sPrNameFee : tez
  ; sRtNameFee : tez
  }

type tTeznameStorage =
  { sProtocol      : string
  ; sTezname       : tHashedName
  ; sParentAddr    : address
  ; sGateAddr      : address
  (* -- ownership -- *)
  ; sDest          : string
  ; sDestType      : tDestType
  ; sOwner         : address
  (* -- advanced info -- *)
  ; sTeznameType    : tTeznameType
  ; sFees           : tFees
  ; sSubnameRecords : tRecords
  (* -- temporal info -- *)
  ; sRegistrationDate  : timestamp
  ; sExpirationDate    : timestamp
  ; sLastModification  : timestamp * string
  }

contract type TeznameSpec = sig
  type storage
  val%entry hasChild : bytes -> _
  val%entry noChild : bytes -> _
  val%entry hasParent : address -> _
  val%entry levelCheck : tTeznameType -> _
  val%entry oneYr : unit -> _
  val%entry pay : tTeznameType -> _
  val%entry updateSubnameRecord : (bytes  * address * tDestType) -> _
  val%entry updateOwnership : (string * tDestType * tTeznameType * tFees) -> _
  val%entry renew : unit -> _
  val%entry withdraw : UnitContract.instance -> _
end

let[@inline] type2int _TeznameType : nat =
  begin match _TeznameType with
    | Regular -> 0p
    | Premium -> 1p
    | Restricted _ -> 2p
  end

(* >>> >>> >>> >>> >>> >>> >>> *)
contract Tezname = struct

  type storage = tTeznameStorage

  (* ## contract entry ## *)
  (* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

  let%entry hasChild (_hname : bytes) s =
    if (not (Map.mem _hname s.sSubnameRecords))
    then Current.failwith "[ERROR]hasChild";
    ([] : operation list) , s

  let%entry noChild (_hname : bytes) s =
    if (Map.mem _hname s.sSubnameRecords)
    then Current.failwith "[ERROR]hasChild";
    ([] : operation list) , s

  let%entry hasParent (_addr : address) s =
    if (_addr <> s.sParentAddr)
    then Current.failwith "[ERROR]hasParent";
    ([] : operation list) , s

  let%entry levelCheck ( _TeznameType : tTeznameType) s =
    let requestLv = type2int _TeznameType in
    let currentLv = type2int s.sTeznameType in
    if (requestLv < currentLv)
      then Current.failwith "[ERROR]levelCheck/0";
    if ((requestLv = currentLv) && (Current.time () < s.sExpirationDate))
      then Current.failwith "[ERROR]levelCheck/1";
    ([] : operation list) , s

  let%entry oneYr ( _ : unit ) s =
    let deltaTime = (Current.time ()) - s.sExpirationDate in
    if (deltaTime < oneYear) then Current.failwith "[ERROR]payRenew2";
    ([] : operation list) , s

  let%entry pay
    ( _TeznameType : tTeznameType ) s =
      let (p1,p2,p3) = s.sFees in
      let price =
        begin match _TeznameType with
          | Regular -> p1
          | Premium -> p2
          | Restricted _ -> p3
        end in
      if ((Current.amount ()) < price)
      then Current.failwith "[ERROR]purchase/1";
      ([] : operation list) , s

  let%entry updateSubnameRecord
    ( _hname , _addr   , _DestType
    : bytes  * address * tDestType ) s =
      let s = s.sSubnameRecords <-
        Map.update _hname (Some (_addr, _DestType)) s.sSubnameRecords in
      ([] : operation list) , s

  let%entry updateOwnership
    ( _Dest , _DestType , _TeznameType , _Fees
    : string * tDestType * tTeznameType * tFees) s =
      if (Current.sender () <> s.sParentAddr)
      then Current.failwith "[ERROR]updateOwner/1";
      let now = Current.time () in
      let s = s.sDest             <- _Dest in
      let s = s.sDestType         <- _DestType in
      let s = s.sTeznameType      <- _TeznameType in
      let s = s.sFees             <- _Fees in
      let s = s.sRegistrationDate <- now in
      let s = s.sExpirationDate   <- now + oneYear in
      let s = s.sLastModification <- (now, "Registration") in
      let s = s.sOwner            <- (Current.source ()) in
      ([] : operation list) , s

  let%entry renew (_ : unit) s =
    if (Current.sender () <> s.sGateAddr)
    then Current.failwith "[ERROR]renew/0";
    if (Current.source () <> s.sOwner)
    then Current.failwith "[ERROR]renew/1";
    if (Current.time () >= s.sExpirationDate) then Current.failwith "[ERROR]renew/2";
    let s = s.sExpirationDate  <- (s.sExpirationDate + oneYear) in
    let s = s.sLastModification <- (Current.time (), "Renew") in
    ([] : operation list) , s

  let%entry withdraw (wallet : UnitContract.instance) s =
    if (s.sOwner <> Current.source ())
    then Current.failwith "[ERROR]withdraw/0";
    let op = Contract.call wallet (Current.balance ()) () in
    ([op] : operation list) , s

end (* end of contract Tezname *)
(* <<< <<< <<< <<< <<< <<< <<< *)

type storage =
  { sGate  : address
  ; sAdmin : address
  }

let%entry deploy
  ( _hname, _Dest  , _DestType, _TeznameType  , _fees , _manager , _parentAddr , _gateAddr
  : bytes * string * tDestType * tTeznameType * tFees * key_hash * address * address) s =
    if (Current.amount () <> 0tz) then Current.failwith "[ERROR]deploy/0";
    if (Current.sender () <> s.sGate)   then Current.failwith "[ERROR]deploy/1";
    let now = Current.time () in
    let stg =
      { sProtocol      = "9chsTNS-0.1"
      ; sTezname       = _hname
      ; sParentAddr    = _parentAddr
      ; sGateAddr      = s.sGate
      (* .. *)
      ; sDest          = _Dest
      ; sDestType      = _DestType
      ; sOwner         = Current.source ()
      (* .. *)
      ; sTeznameType    = _TeznameType
      ; sFees           = _fees
      ; sSubnameRecords = (Map [] : tRecords)
      (* .. *)
      ; sRegistrationDate = now
      ; sExpirationDate   = now
      ; sLastModification = now , "Deployment"
      } in
    let (op1, addr) = Contract.create
      ~storage:stg
      ~manager:_manager
      ~spendable:false
      ~delegatable:false
      ~delegate:None
      ~amount:0tz
      (contract Tezname) in
    [op1] , s
