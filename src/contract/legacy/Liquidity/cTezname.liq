(* ## constant ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

let[@inline] oneYear  = 31556926
let[@inline] oneMonth =  2592000
let[@inline] regularP =   10.0tz

(* ## type ## *)
(* ↓↓↓↓↓↓↓↓↓↓ *)

type tHashedName = bytes
type tRecords = (tHashedName, address) map
type tMerkleProof = ((bytes, bytes) variant) list
type tTeznameType
  = Regular | Premium | Restricted of bytes

type tTeznameStorage =
  { sTezname       : tHashedName
  ; sAdmin         : address
  (* -- ownership -- *)
  ; sDest          : address
  ; sOwner         : address
  (* -- advanced info -- *)
  ; sTeznameType    : tTeznameType
  ; sSubnameRecords : tRecords
  (* -- temporal info -- *)
  ; sRegistrationDate  : timestamp
  ; sExpirationDate    : timestamp
  ; sLastModification  : timestamp * string
  }

(* ## contract definition ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

contract type TeznameSpec = sig
  type storage
  val%entry resetOwnership : (address * address * tTeznameType) -> _
  val%entry updateSubnameRecord : (bytes * address) -> _
  val%entry update : ((address, address) variant) -> _
  val%entry renew : unit -> _
end

type storage = tTeznameStorage

(* ## contract entry ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

(* role : admin *)
let%entry resetOwnership
  ( _Owner  , _Dest   , _TeznameType
  : address * address * tTeznameType ) s =
    (* ### admin identity check ### *)
    if (Current.source () <> s.sAdmin)
    then Current.failwith "[ERROR]updateOwnership/admin";
    (* ### update ownership ### *)
    let s = s.sOwner            <- _Owner in
    let s = s.sDest             <- _Dest in
    let s = s.sTeznameType      <- _TeznameType in
    let now = Current.time () in
    let s = s.sRegistrationDate <- now in
    let s = s.sExpirationDate   <- now + oneYear in
    let s = s.sLastModification <- (now, "Registered") in
    ([] : operation list) , s

(* role : admin *)
let%entry updateSubnameRecord
  ( _hname , _addr
  : bytes  * address ) s =
    (* ### admin identity check ### *)
    if (Current.source () <> s.sAdmin)
    then Current.failwith "[ERROR]updateSubnameRecord/admin";
    let s = s.sSubnameRecords <-
      Map.update _hname (Some _addr) s.sSubnameRecords in
    ([] : operation list) , s

(* role : owner *)
let%entry update (input : (address, address) variant) s =
  if (Current.source () <> s.sOwner)
  then Current.failwith "[ERROR]update/not_owner";
  let s = begin match input with
    | (Left newOwner) -> let p = (s.sOwner <- newOwner) in p
    | (Right newDest) -> let p = (s.sDest  <- newDest) in p
  end in
  let now = Current.time () in
  let s = s.sLastModification <- (now, "Updated") in
  ([] : operation list) , s

(* role : owner *)
let%entry renew (_ : unit) s =
  if (Current.source () <> s.sOwner)
  then Current.failwith "[ERROR]renew/owner";
  let amo = Current.amount () in
  if (amo < regularP)
  then Current.failwith "[ERROR]renew/insufficiency";
  let now = Current.time () in
  let renewableDate = s.sExpirationDate - oneMonth in
  if (now > s.sExpirationDate) || (now < renewableDate)
  then Current.failwith "[ERROR]renew/invalid_date";
  let s = s.sExpirationDate <- s.sExpirationDate + oneYear in
  let s = s.sLastModification <- (now, "Updated") in
  ([] : operation list) , s
