(* ## type ## *)
(* ↓↓↓↓↓↓↓↓↓↓ *)

type tMerkleProof = ((bytes, bytes) variant) list
type tTeznameInfo
  = RegularName
  | PremiumName    of tMerkleProof
  | RestrictedName of tMerkleProof * bytes
type tMerkleRoot = bytes

type tNameTypesStorage =
  { sAdmin          : address
  ; sPremiumRoot    : tMerkleRoot
  ; sRestrictedRoot : tMerkleRoot
  }

(* ## local function ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

let[@inline] step (h, acc) =
  match h with
  | Left x -> Crypto.blake2b (x @ acc)
  | Right x -> Crypto.blake2b (acc @ x)

(* ## contract definition ## *)
(* ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ *)

type storage = tNameTypesStorage

let%entry nameCheck
  ( _hname , _TeznameInfo : bytes  * tTeznameInfo ) s =
  begin match _TeznameInfo with
    | RegularName ->
      ([] : operation list) , s
    | PremiumName proof ->
      let merkleVal = List.fold step proof _hname in
      if(merkleVal <> s.sPremiumRoot)
      then Current.failwith "[ERROR]checkInfo/0";
      ([] : operation list) , s
    | RestrictedName (proof, _) ->
      let merkleVal = List.fold step proof _hname in
      if(merkleVal <> s.sRestrictedRoot)
      then Current.failwith "[ERROR]checkInfo/1";
      ([] : operation list) , s
  end

let%entry update
  ( a, b : tMerkleRoot * tMerkleRoot) s =
  if (Current.source () <> s.sAdmin)
  then Current.failwith "[ERROR]update/admin";
  let s = s.sPremiumRoot <- a in
  let s = s.sRestrictedRoot <- b in
  ([] : operation list) , s
