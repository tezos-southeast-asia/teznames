#!/bin/bash
#
if [ "$1" == "" ]; then
  basedir=$(git rev-parse --show-toplevel) || exit 1
else
  basedir="$1"
fi
echo -e "\033[36m[info]\033[39m set basedir as ${basedir}"
#
echo -e "\033[36m[info]\033[39m loading ${basedir}/ENV.."
source "${basedir}/ENV" || exit 1
#
echo -e "\033[36m[info]\033[39m reseting ${basedir}/ADDR.."
if [ -f "${basedir}/ADDR" ]; then
  rm "${basedir}/ADDR"
  touch "${basedir}/ADDR"
else
  touch "${basedir}/ADDR"
fi
#
# =====================
# ===== nametypes =====
# =====================
#
cname=cNameTypes
#
initS=""
# setup admin address
initS+="(Pair \"${admin}\""
# setup the root byte of premium merkle tree
initS+="(Pair 0x05ea57164837f0314f1eeda6ff84a3e54fa85e69cf11adaece6def27239ab971 "
# setup the root byte of restricted merkle tree
initS+="0xc12823a002f9c62d0ec17fa65b1585e9b8af51465ca7fbfb350bbd1234c4c58e))"
#
echo -e "\033[36m[info]\033[39m deploying ${cname}-${version}.."
#
contractName="${cname}-${version}"
#
/nix/store/bgqva3wgi3knivdk9pf7gdd0384hj2qf-tezos-0.0.0/bin/tezos-client -A "${tezosnode}"\
  originate contract "${contractName}" \
  transferring 0 from "$admin" \
  running "${contractdir}/${cname}.tz" \
  --init "$initS" \
  --burn-cap 16.0 \
  --force \
  > "${logdir}/${contractName}.deployment" 2>&1 || exit 1
sleep 1
#
nameTypesAddr=$(grep "New contract" "${logdir}/${contractName}.deployment" | awk '{print $3}') || exit 1
echo -e "NAMETYPESADDR=${nameTypesAddr}" >> "${basedir}/ADDR"
echo -e "\033[36m[info]\033[39m \033[33m${contractName}\033[39m has been originated at \033[32m$nameTypesAddr\033[39m"
#
# ====================
# ===== rootname =====
# ====================
#
cname=cTezname
#
# the Raw Sha256 hash of "tez"
rootname=0xee89064a5aae6eb7cc3a418829d628329ddcbfacb5a667524245d92eed96f5ee
#
# setup default tri-prices for rootname
initS=""
initS+="(Pair ${rootname} "  # %sTezname
initS+="(Pair \"${admin}\" " # %sAdmin
initS+="(Pair \"${admin}\" " # %sDest
initS+="(Pair \"${admin}\" " # %sOwner
initS+="(Pair (Right (Right ${rootname})) " # %sTeznameType
initS+="(Pair {} "           # %sSubnameRecords
initS+="(Pair \"2019-10-01T00:00:01Z\" "    # %sRegistrationDate
initS+="(Pair \"2119-10-01T00:00:01Z\" "    # %sExpirationDate
initS+="(Pair \"2019-10-01T00:00:01Z\" \"GENESIS\")" # %sLastModification
initS+="))))))))"
#
echo -e "\033[36m[info]\033[39m deploying rootname ${cname}-${version}.."
#
contractName="rootname-${version}"
#
/nix/store/bgqva3wgi3knivdk9pf7gdd0384hj2qf-tezos-0.0.0/bin/tezos-client -A "${tezosnode}" \
  originate contract "${contractName}" \
  transferring 0 from "$admin" \
  running "${contractdir}/${cname}.tz" \
  --init "$initS" \
  --burn-cap 16.0 \
  --force \
  > "${logdir}/${contractName}.deployment" 2>&1 || exit 1
sleep 1
rootnameAddr=$(grep "New contract" "${logdir}/${contractName}.deployment" | awk '{print $3}') || exit 1
echo -e "ROOTNAMEADDR=${rootnameAddr}" >> "${basedir}/ADDR"
echo -e "\033[36m[info]\033[39m \033[33m${contractName}\033[39m has been originated at \033[32m$rootnameAddr\033[39m"
#
# ================
# ===== gate =====
# ================
#
cname=cGate
#
comm="(Pair 10000000 (Pair 50000000 150000000))"
#
initS=""
initS+="(Pair \"${admin}\" "         # %sAdmin
initS+="(Pair \"${nameTypesAddr}\" " # %scNameTypes
initS+="(Pair \"${rootnameAddr}\" "  # %scRootname
initS+="(Pair ${comm} "  # %sCommission
initS+="{ Elt \"someone.tez\" "
initS+="(Pair \"${admin}\" (Pair 10000000 \"2019-10-01T00:00:01Z\")) } "       # %sAuctions
initS+="))))"
#
echo -e "\033[36m[info]\033[39m deploying ${cname}-${version}.."
#
contractName="${cname}-${version}"
#
/nix/store/bgqva3wgi3knivdk9pf7gdd0384hj2qf-tezos-0.0.0/bin/tezos-client -A "${tezosnode}"\
  originate contract "${contractName}" \
  transferring 0 from "$admin" \
  running "${contractdir}/${cname}.tz" \
  --init "$initS" \
  --burn-cap 16.0 \
  --force \
  > "${logdir}/${contractName}.deployment" 2>&1 || exit 1
sleep 1
gateAddr=$(grep "New contract" "${logdir}/${contractName}.deployment" | awk '{print $3}') || exit 1
echo -e "GATEADDR=${gateAddr}" >> "${basedir}/ADDR"
echo -e "\033[36m[info]\033[39m \033[33m${contractName}\033[39m has been originated at \033[32m$gateAddr\033[39m"
#
echo -e "\033[36m[info]\033[39m core contracts have been deployed successfully"
