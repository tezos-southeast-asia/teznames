This document contains a list of RESTful API services for _core contracts version 0.4_.

## Serving!

Server url: `https://api.tezos.id/mooncake/babylonnet/teznames/v0.4/babylon`.

### Tezname Status

!!! summary "Check existence"

    ```
    GET /status/isRegistered
    ```

    | Attribute | Type   | Required | Description  |
    | --------- | ------ | -------- | ------------ |
    | name      | string | yes      | full tezname |

    Example: `/status/isRegistered?name=9chs.tez`

    **Response**

    ??? success "Success Response (200)"

        | Field        | Type | Description   |
        | ------------ | ---- | ------------- |
        | isRegistered | bool | is registered |

        Example response:

        ```JSON
        { "isRegistered" : true }
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Check expiration date"

    ```
    GET /status/isExpired
    ```

    | Attribute | Type   | Required | Description  |
    | --------- | ------ | -------- | ------------ |
    | name      | string | yes      | full tezname |

    Example: `/status/isExpired?name=9chs.tez`

    **Response**

    ??? success "Success Response (200)"

        | Field      | Type | Description |
        | ---------- | ---- | ----------- |
        | isExpired  | bool | is expired  |
        | isExisting | bool | is existing |

        Example response:

        ```JSON
        { "isExisting" : true
        , "isExpired"  : false
        }
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Check registrability"

    ```
    GET /status/isRegisterable
    ```

    | Attribute | Type   | Required | Description  |
    | --------- | ------ | -------- | ------------ |
    | name      | string | yes      | full tezname |

    Example: `/status/isRegisterable?name=9chs.tez`

    **Response**

    ??? success "Success Response (200)"

        | Field          | Type   | Description             |
        | -------------- | ------ | ----------------------- |
        | diffTime       | number | exp time - now (in sec) |
        | isRegisterable | bool   | is registerable         |

        Example response:

        ```JSON
        { "diffTime"       : 28683712
        , "isRegisterable" : false
        }
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

### Tezname Info

!!! summary "Retrieve Tezname address"

    ```
    GET /info/getTeznameAddr
    ```

    | Attribute | Type   | Required | Description  |
    | --------- | ------ | -------- | ------------ |
    | name      | string | yes      | full tezname |

    Example: `/info/getTeznameAddr?name=9chs.tez`

    **Response**

    ??? success "Success Response (200)"

        | Field       | Type   | Description              |
        | ----------- | ------ | ------------------------ |
        | teznameAddr | string | tezname contract address |

        Example response:

        ```JSON
        {"teznameAddr" : "KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve Owner address"

    ```
    GET /info/getOwner
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/getOwner?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field | Type   | Description   |
        | ----- | ------ | ------------- |
        | owner | string | owner address |

        Example response:

        ```JSON
        {"owner" : "KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve Tezname destination"

    ```
    GET /info/getDest
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/getDest?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field | Type   | Description |
        | ----- | ------ | ----------- |
        | dest  | string | destination |

        Example response:

        ```JSON
        {"dest" : "tz1UNufAZHvrD4NZXLLjt3ZhKUdz89kZdgU3"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve Tezname destination from a given Tezname"

    ```
    GET /info/getDestFromName
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | name      | string | yes      | full tezname        |

    Example: `/info/getDestFromName?name=tsa.tez`

    **Response**

    ??? success "Success Response (200)"

        | Field | Type   | Description |
        | ----- | ------ | ----------- |
        | dest  | string | destination |

        Example response:

        ```JSON
        {"dest" : "tz1UNufAZHvrD4NZXLLjt3ZhKUdz89kZdgU3"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "List address of all subnames"

    ```
    GET /info/getSubnames
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/getSubnames?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field   | Type   | Description      |
        | ------- | ------ | ---------------- |
        | hname   | string | subname          |
        | address | string | contract address |

        Example response:

        ```JSON
        [
          { "name" : "932a047d7dc6c95691f626a6b349eb3833d2c2a66838820fd09eae89e4865716"
          , "address" : "KT1XpjLtiHGqzZmC9W58kmgpkKJxiSDsqk5s"
          },
          { "name" : "4f29f773b7a34ae32ec32183c180d5531ad9313438cd9b25220900fdb95bc271"
          , "address" : "KT1NuLNFhxUfc1TA1uDsdRsZN6myjo1nLLeB"
          },
          { "name" : "4a3720151f1319d8372b181d6298fb265a7cd3f8fd28e19393838f87a526c73e"
          , "address" : "KT1GcufNfMMq3XHvX52v8N1grJXjsvS28AWr"
          }
        ]
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "List all registration info"

    ```
    GET /info/registrationList
    ```

    **Response**

    ??? success "Success Response (200)"

        | Field     | Type   | Description          |
        | --------- | ------ | -------------------- |
        | name      | string | full tezname         |
        | address   | string | registrant's address |
        | amount    | string | paid amount (mutez)  |
        | timestamp | string | registration time    |

        Example response:

        ```JSON
        [
            { "amount" : "10000000"
            , "address" : "tz1bWXYwhaTLnBeGH54CnTYiThZ6tP9nhy4Y"
            , "name" : "someone.tez"
            , "timestamp" : "2019-10-01T00:00:01Z"
            },
            { "amount" : "10000000"
            , "address" : "tz1QoVdz63AWJ55Sm7veaQxW929LTugapL9y"
            , "name" : "yolo.tez"
            , "timestamp" : "2019-10-25T10:09:08Z"
            }
        ]
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve all timestamps"

    ```
    GET /info/timestamps
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/timestamps?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field            | Type   | Description            |
        | ---------------- | ------ | ---------------------- |
        | registrationDate | string | registration date      |
        | expirationDate   | string | expiration date        |
        | modificationDate | string | last modification date |
        | modificationText | string | last modification Info |

        Example response:

        ```JSON
        { "registrationDate" : "2019-10-01T00:00:01Z"
        , "expirationDate"   : "2119-10-01T00:00:01Z"
        , "modificationDate" : "2019-10-01T00:00:01Z"
        , "modificationText" : "GENESIS"
        }
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve registration time"

    ```
    GET /info/timestamps/getRegDate
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/timestamps/getRegDate?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field            | Type   | Description            |
        | ---------------- | ------ | ---------------------- |
        | registrationDate | string | registration date      |

        Example response:

        ```JSON
        {"registrationDate":"2019-10-01T00:00:01Z"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve expiration time"

    ```
    GET /info/timestamps/getExpDate
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/timestamps/getExpDate?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field            | Type   | Description            |
        | ---------------- | ------ | ---------------------- |
        | expirationDate   | string | expiration date        |

        Example response:

        ```JSON
        {"expirationDate":"2119-10-01T00:00:01Z"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Retrieve the last modification info"

    ```
    GET /info/timestamps/getModDate
    ```

    | Attribute | Type   | Required | Description         |
    | --------- | ------ | -------- | ------------------- |
    | address   | string | yes      | contract of tezname |

    Example: `/info/timestamps/getModDate?address=KT1D85ihgjSd77oqiUmQmeW6mEuxHpN36b6E`

    **Response**

    ??? success "Success Response (200)"

        | Field            | Type   | Description            |
        | ---------------- | ------ | ---------------------- |
        | modificationDate | string | last modification date |

        Example response:

        ```JSON
        {"modificationDate":"2019-10-01T00:00:01Z"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

--


### Utility

!!! summary "Retrieve system gate address"

    ```
    GET /getGateAddr
    ```

    **Response**

    ??? success "Success Response (200)"

        | Field    | Type   | Description  |
        | -------- | ------ | ------------ |
        | gateAddr | string | gate address |

        Example response:

        ```JSON
        {"gateAddr" : "KT1AVZKhHkaf1h8ewT2JbpXC49u8xP1K6erz"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

!!! summary "Hash case insensitive word"

    ```
    GET /hashWord
    ```

    | Attribute | Type   | Required | Description           |
    | --------- | ------ | -------- | --------------------- |
    | word      | string | yes      | case insensitive word |

    Example: `/info/hashWord?word=happy`

    **Response**

    ??? success "Success Response (200)"

        | Field | Type   | Description |
        | ----- | ------ | ----------- |
        | hword | string | hashed word |

        Example response:

        ```JSON
        {"hword" : "c69bf80ef1df6293c1f73ad675f270ef3bf97fcb85e6dffdaf3526769bff6458"}
        ```

    ??? failure "Error response"

        | Code | Description       |
        | ---- | ----------------- |
        | 400  | missing parameter |
        | 422  | other error       |

## Coming soon!

| Function                         | Description                                 |
| -------------------------------- | ------------------------------------------- |
| `GET /status`                    | Retrieve general status info                |
| `GET /status/isRegularName`      | Check if name is regular name               |
| `GET /getRegularPrice`           | Retrieve registration price of regular name |
